package com.example.grpcservice;

import com.example.grpc.Employee;
import com.example.grpc.EmployeeServiceGrpc;
import com.example.grpc.Empty;
import com.example.grpc.TermDB;
import io.grpc.stub.StreamObserver;
import net.devh.boot.grpc.server.service.GrpcService;

import java.nio.file.attribute.UserPrincipalNotFoundException;

@GrpcService
public class EmployeeServiceService extends EmployeeServiceGrpc.EmployeeServiceImplBase {

    @Override
    public void getEmployee(Employee request, StreamObserver<Employee> responseObserver) {

        TermDB.getEmployeesFromMockDb()
                .stream()
                .filter(em -> em.getEmployeeId() == request.getEmployeeId())
                .findFirst()
                .ifPresentOrElse(
                        responseObserver::onNext,
                        () -> responseObserver.onError(new UserPrincipalNotFoundException("Not exist employee"))
                );

        responseObserver.onCompleted();
    }

    @Override
    public void getEmployees(Empty request, StreamObserver<Employee> responseObserver) {
        TermDB.getEmployeesFromMockDb()
                .forEach(responseObserver::onNext);

        responseObserver.onCompleted();
    }
}
