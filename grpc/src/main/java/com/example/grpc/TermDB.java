package com.example.grpc;

import java.util.ArrayList;
import java.util.List;

public class TermDB {
    public static List<Employee> getEmployeesFromMockDb() {
        return new ArrayList<>() {
            {
                add(Employee.newBuilder().setEmployeeId(1).setCompanyId(1).setName("Tien").setAge(20).setAddress("Ha Noi").build());
                add(Employee.newBuilder().setEmployeeId(2).setCompanyId(2).setName("Hoang Nam").setAge(27).setAddress("Quang Ninh").build());
                add(Employee.newBuilder().setEmployeeId(3).setCompanyId(3).setName("Tuan").setAge(23).setAddress("Nghe An").build());
                add(Employee.newBuilder().setEmployeeId(4).setCompanyId(4).setName("Phuong").setAge(22).setAddress("Hai Phong").build());
            }
        };
    }

    public static List<Company> getCompanyFromMockDb() {
        return new ArrayList<Company>() {
            {
                add(Company.newBuilder().setCompanyId(1).setName("MIGI").setScale("Nho").setAddress("My Dinh").build());
                add(Company.newBuilder().setCompanyId(2).setName("FPT").setScale("Lon").setAddress("Hoa Lac").build());
                add(Company.newBuilder().setCompanyId(3).setName("CMC").setScale("Lon").setAddress("Nam Tu Liem").build());
                add(Company.newBuilder().setCompanyId(4).setName("Unicloud").setScale("TB").setAddress("Nam Tu Liem").build());
                add(Company.newBuilder().setCompanyId(5).setName("VIETIS").setScale("TB").setAddress("My Dinh").build());
            }
        };
    }
}
