package com.example.client.controller;

import com.example.client.service.EmployeeClientService;
import com.google.protobuf.Descriptors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class EmployeeController {
    private final EmployeeClientService employeeClientService;

    public EmployeeController(EmployeeClientService employeeClientService) {

        this.employeeClientService = employeeClientService;
    }

    @GetMapping(value = "/employee/{id}")
    public Map<Descriptors.FieldDescriptor, Object> getEmployee(@PathVariable Integer id) {

        return employeeClientService.getEmployee(id);
    }

    @GetMapping(value = "/employee")
    public List<Map<Descriptors.FieldDescriptor, Object>> getAllEmployees() {

        return employeeClientService.getEmployees();
    }
}
