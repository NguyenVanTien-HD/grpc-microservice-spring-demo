package com.example.client.service;

import com.google.protobuf.Descriptors;

import java.util.List;
import java.util.Map;

public interface IEmployeeClientService {
    Map<Descriptors.FieldDescriptor, Object> getEmployee(int employeeId);
    List<Map<Descriptors.FieldDescriptor, Object>> getEmployees();
}
