package com.example.client.service;

import com.example.grpc.Employee;
import com.example.grpc.EmployeeServiceGrpc;
import com.example.grpc.Empty;
import com.google.protobuf.Descriptors;
import net.devh.boot.grpc.client.inject.GrpcClient;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class EmployeeClientService implements IEmployeeClientService{

    @GrpcClient("grpc-demo-service")
    EmployeeServiceGrpc.EmployeeServiceBlockingStub stub;

    public Map<Descriptors.FieldDescriptor, Object> getEmployee(int employeeId) {
        Employee employee = Employee.newBuilder().setEmployeeId(employeeId).build();
        Employee response = stub.getEmployee(employee);

        return response.getAllFields();
    }

    @Override
    public List<Map<Descriptors.FieldDescriptor, Object>> getEmployees() {
        List<Map<Descriptors.FieldDescriptor, Object>> maps = new ArrayList<>();

        stub.getEmployees(Empty.newBuilder().build())
                .forEachRemaining(employee -> maps.add(employee.getAllFields()));

        return maps;
    }
}
